import * as React from 'react';
import {HashRouter as Router, Route, Switch} from 'react-router-dom'
import {Home} from './Home';
/* tslint:disable */
const settings = require('../../settings/client.json')

export class App extends React.Component {
	componentDidMount(): void {
		document.title = settings.name;
	}
	/**
	 *  Render method
	 */
	render(): JSX.Element {
		return <div className='app-container'>
			<Router>
				<Switch>
					<Route exact path='/' component={Home} />
				</Switch>
			</Router>
		</div>
	}
}
export default App;