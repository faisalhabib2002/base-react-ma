import * as React from 'react';
import * as ReactDOM from 'react-dom';
require('../style/index.less')
// Style files for typically used libararies
// import 'antd/dist/antd.less';
// import '@simplus/siui/style/index.less'
// import '@simplus/macaw-business/style/index.less'

// Robin methods
// import {robins} from 'src/robins'
// import {RobinProvider} from '@simplus/robin'
// import RobinReact from '@simplus/robin-react'
// const provider = new RobinProvider(robins);
// RobinReact.setProvider(provider);

import {App} from 'src/components';

ReactDOM.render(<App/>, document.getElementById('my-awesome-app'))